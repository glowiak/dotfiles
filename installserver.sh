#!/bin/sh
# exports
export NAME="Minecraft Server"
export SERVER_VERSION="0.1.3"
export VERSION="1.0.16_02"
export SERVER_URL="http://betacraft.pl/server-archive/minecraft/a0.1.3.jar"
export SERVER_DIR="$HOME/minecraft/server/$VERSION"
export DOWNLOAD_TO="$SERVER_DIR/server.jar"
export JAVA_HOME=/usr/local/openjdk8

# functions
try_java()
{
	echo -n "Testing Java..."
	if [ ! -f "$JAVA_HOME/jre/bin/java" ]
	then
		echo "Java cannot be found!"
		exit 1
	fi
	echo "Found Java at $JAVA_HOME/jre/bin/java!"
}

welcome_msg()
{
	clear
	echo "$NAME Installer"
	echo "=========================="
	echo ""
	echo "Welcome to $NAME Installer!"
	echo "Press enter key to install $NAME to $SERVER_DIR..."
	read psq1
	download_server_files
	setup_server_memory
	echo "Server setup complete!"
	echo "Type '$0 start' to start the server!"
	exit 0
}

download_server_files()
{
	echo "Downloading $NAME version $SERVER_VERSION for Minecraft $VERSION..."
	mkdir -p $SERVER_DIR
	fetch --no-check-certificate -o $DOWNLOAD_TO $SERVER_URL
}
setup_server_memory()
{
	echo -n "Amount of memory to allocate for the server: "
	read ServerMemory
	echo "$ServerMemory" > $SERVER_DIR/memory.conf
}
start_server()
{
	try_java
	if [ ! -f "$SERVER_DIR/memory.conf" ]
	then
		echo "ERROR: Server directory doesn't contains memory.conf file, which is needed to start!"
		exit 1
	fi
	export NET_MINECRAFT_SERVER_MEMORY="$(cat $SERVER_DIR/memory.conf)"
	cd $SERVER_DIR
	$JAVA_HOME/jre/bin/java -Xmx$NET_MINECRAFT_SERVER_MEMORY -Xms$NET_MINECRAFT_SERVER_MEMORY -jar $SERVER_DIR/server.jar || exit 0
}
edit_prop()
{
	vi $SERVER_DIR/server.properties
	exit 0
}
case "$1" in
	setup)
		welcome_msg
		;;
	start)
		start_server
		;;
	eprop)
		edit_prop
		;;
	*)
		echo "Usage:"
		echo "To setup the server --> $0 setup"
		echo "To run an existing server --> $0 start"
		echo "To edit server configuration --> $0 eprop"
		exit 0
		;;
esac
